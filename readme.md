# Apache 2 commands #
[Configure](https://www.linode.com/docs/websites/lamp/lamp-on-ubuntu-14-04/) a LAMP server

### service ###
* Common commands
```bash
sudo service apache2 status
sudo service apache2 start
sudo service apache2 stop
sudo service apache2 restart
# Leave service running, but re-read configuration files
sudo service apache2 reload
```

### [Apache Basics](https://www.linode.com/docs/websites/apache-tips-and-tricks/apache-configuration-basics) ###

Folder Locations
* Apache2 configuration files
    * /etc/apache2/
* PHP initialize files
    * /etc/php5/apache2/php.ini


### Apache Logs
* [Log files](https://www.digitalocean.com/community/tutorials/how-to-configure-logging-and-log-rotation-in-apache-on-an-ubuntu-vps)
    * /var/log/apache2/error.log
    * /var/log/apache2/access.log
        * [Log formats](http://httpd.apache.org/docs/current/logs.html#combined)
        * [Simple explanation](http://stackoverflow.com/questions/9234699/understanding-apaches-access-log)
    * Logrotate
        * Configure
            * [Rackspace](https://support.rackspace.com/how-to/understanding-logrotate-utility/)
            * [ServersForHackers](https://serversforhackers.com/c/managing-logs-with-logrotate)
        * [Execute](http://www.thecave.info/execute-logrotate-command-manually/)
            * logrotate -vf /etc/logrotate.d/apache2.conf
            * logrotate --force /path/to/configuration/file


### PHP Sessions ###
* [Location](https://stackoverflow.com/questions/4927850/location-for-session-files-in-apache-php) - /var/lib/php/session


### MySQL Troubleshooting ###
* [MySQL Slow Log](https://www.databasejournal.com/features/mysql/troubleshooting-mysql-performance-issues-using-the-slow-log-and-performance-schema.html)
    * show global variables like '%slow%';
    * Log location: /var/log/mysql/mysql-slow.log
    

### Sites - Virtual Hosts ###
* [Virtual Hosts](https://serversforhackers.com/configuring-apache-virtual-hosts)
    * /etc/apache2/sites-available/
    * /etc/apache2/sites-enabled/
    * [Examples](https://httpd.apache.org/docs/2.4/vhosts/examples.html)
* Enable/disable a virtual host
```bash
a2dissite [site_name] # disable - remove symlink
a2ensite [site_name] # enable - create symlink

# Need to reload configurations after enable/disable
service apache2 reload
```
* [Enable mod_rewrite](http://askubuntu.com/questions/48362/how-to-enable-mod-rewrite-in-apache)
    * [Redirect of sub-domains to https](http://stackoverflow.com/questions/23059463/redirect-all-subdomains-from-http-to-https)
    * [Apache Redirect HTTP to HTTPS using mod_rewrite](https://www.sslshopper.com/apache-redirect-http-to-https.html)



### Errors on start/restart ###
* AH00072: make_sock: could not bind to address 0.0.0.0:80
    * One [solution](http://wiki.apache.org/httpd/CouldNotBindToAddress) is to find and kill the process using the socket
```bash
sudo lsof -i:80
sudo kill pid#
```
* AH00558: domain name
    *[Solution](http://askubuntu.com/questions/329323/problem-with-restarting-apache2) - is that ServerName needs to be specified in configuration file.
        * Should be localhost(127.0.0.1) not 127.0.1.1
```bash
# Specify outside virtual host sections
echo "ServerName localhost" | sudo tee /etc/apache2/conf-available/servername.conf
# Enable by creating symlink to it from the 'enabled' section
sudo a2enconf servername
# Restart Apache
sudo service apache2 restart
```

### Error - Boot Partition is Full ###
*[Clean Up Boot Partition](https://gist.github.com/ipbastola/2760cfc28be62a5ee10036851c654600)